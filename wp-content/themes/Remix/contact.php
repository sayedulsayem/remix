<?php get_header(); ?>
<?php
/*
	Template Name: contact page
 */
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div style="text-align: center;padding-bottom: 60px;">
                    <?php echo do_shortcode( '[contact-form-7 id="42" title="Contact form 1"]' ); ?>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                //wp_nav_menu( array( 'theme_location'	=> 'blog'	) );
                dynamic_sidebar( 'blog' );
                ?>
            </div>
        </div>
    </div>
</section>



<?php get_footer(); ?>