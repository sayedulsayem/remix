<?php
$serviceList=cs_get_option('service_list');
foreach ($serviceList as $item=>$value){
    ?>
    <div class="col-md-3 col-sm-6">
        <div class="item-md3 wow fadeInLeft" data-wow-duration=".5s" data-wow-delay=".9s">
            <div class="<?php echo $value['service_icon']; ?>"></div>
            <div class="item-info">
                <h4 class="item-title">
                    <a href="#"><?php echo $value['service_title']; ?></a>
                </h4><!-- /.item-title -->
                <p><?php echo $value['service_des']; ?></p>
            </div>
        </div><!-- /.item-md3 -->
    </div><!-- /.col-md-3 -->
<?php } ?>


<div class="col-md-3 col-sm-6">
    <div class="item-md3 wow fadeInRight" data-wow-duration=".5s" data-wow-delay="1.7s">
        <div class="icon flaticon-computer261"></div>
        <div class="item-info">
            <h4 class="item-title">
                <a href="#">Web Design</a>
            </h4><!-- /.item-title -->
            <p>Uniquely maximize equity invested business with maintainable schemas.</p>
        </div>
    </div><!-- /.item-md3 -->
</div><!-- /.col-md-3 -->



<!--widget-->
<div class="col-md-4">
    <div class="sidebar">

        <!-- Search Bar -->
        <div class="widget blog-search-bar">
            <h2 class="rounded">Search</h2>

            <form class="form-search" method="get" id="s" action="/">
                <div class="input-append">
                    <input class="form-control input-medium search-query" type="text" name="s" placeholder="Search" required>
                    <button class="add-on" type="submit"><i class="fa fa-search"></i></button>
                </div><!-- /.input-append -->
            </form><!-- /.form-search -->
        </div><!-- /.blog-search-bar -->
        <!-- Search Bar End -->

        <!-- Blog Categories -->
        <div class="widget blog-categories">
            <div class="catagorie-list">
                <h2 class="rounded">Categories</h2>
                <ul class="cat-list">
                    <li><a href="#">Web Design <span class="pull-right">333</span></a></li>
                    <li><a href="#">WordPress Themes <span class="pull-right">245</span></a></li>
                    <li><a href="#">Graphic Design <span class="pull-right">450</span></a></li>
                    <li><a href="#">PSD Template <span class="pull-right">300</span></a></li>
                </ul><!-- /.cat-list -->
            </div><!-- /.catagorie-list -->
        </div><!-- /.blog-categories -->
        <!-- Blog Categories End -->

        <!-- Popular-post -->
        <div class="popular-post">
            <div class="widget">
                <h2 class="rounded">Latest Posts</h2>

                <div class="latest-post">
                    <div class="inner-item clearfix">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small01.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->

                    <div class="inner-item">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small02.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->

                    <div class="inner-item">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small03.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->
                </div><!-- /.latest-post -->
            </div><!-- /.widget -->
        </div><!-- /.popular-post -->
        <!-- Popular-post End -->

        <!-- Flicker Widget -->
        <div class="widget">
            <h2 class="rounded">Flicker Images</h2>
            <div class="widget-gallery">
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small5.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small6.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small7.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small8.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small6.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small7.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small8.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
                <div class="single-gallery">
                    <a href="#"><img class="img-responsive" src="images/post/small5.jpg" alt="Gallery Image"></a>
                </div><!-- /.single-gallery -->
            </div><!-- /.message -->
        </div><!-- /.widget -->
        <!-- Flicker Widget End -->

        <!-- Popular-post -->
        <div class="popular-post">
            <div class="widget">
                <h2 class="rounded">Popular Posts</h2>

                <div class="latest-post">
                    <div class="inner-item clearfix">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small01.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->

                    <div class="inner-item">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small02.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->

                    <div class="inner-item">
                        <div class="post-img">
                            <a href="single.php"><img class="img-responsive" src="images/post/small03.jpg" alt="Post Iamge"></a>
                        </div><!-- /.auther-img -->

                        <div class="post-details">
                            <p class="message">
                                <a href="single.php">Ssively administratdologies with sca able infomediaries. Assertively</a>
                            </p>
                            <p class="entry-meta">
                                <a href="#">2 Comments</a> <span>at</span>
                                <a href="#"> 2:05 am</a>
                            </p>
                        </div><!-- /.post-details -->
                    </div><!-- /.inner-item -->
                </div><!-- /.latest-post -->
            </div><!-- /.widget -->
        </div><!-- /.popular-post -->
        <!-- Popular-post End -->

        <!-- Tag Widget -->
        <div class="widget">
            <div class="tag-clouds">
                <h2 class="rounded">Tag Clouds</h2>

                <div class="tag-list">
                    <a href="#">Design</a>
                    <a href="#">Development</a>
                    <a href="#">SEO</a>
                    <a href="#">Graphic</a>
                    <a href="#">WordPress</a>
                    <a href="#">Art</a>
                    <a href="#">Social Media</a>
                    <a href="#">Photoshop</a>

                </div><!-- /.tag-list -->
            </div><!-- /.tag-clouds -->
        </div>
        <!-- end Tag Widget -->

    </div><!-- /.sidebar -->
</div><!-- /.col-md-4 -->

