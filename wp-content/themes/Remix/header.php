<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Remix</title>

    <meta name="description" content="Remix - is a Free HTML Responsive Templeate by PortfolioBazaar Team. You can use this for anykind of organaizations">

    <meta name="keywords" content="Free HTML Template">

    <meta name="author" content="PortfolioBazaar">

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="10" <?php body_class(); ?>>

<!-- Main Menu -->
<div id="main-menu"  class="main-menu-container navbar-fixed-top">
    <div  class="main-menu">
        <div class="container">
            <div class="row">
                <div class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button><!-- /.navbar-toggle collapsed -->
                            <a class="navbar-brand" href="<?php echo home_url(); ?>"><img class="logo-nav" src="<?php echo cs_get_option('upload_logo'); ?>" alt="Logo Image"></a>
                        </div><!-- /.navbar-header -->

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <nav class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">

<!--	                        --><?php //wp_nav_menu(array(
//		                        'theme_location' => 'remix_header_menu',
//		                        'menu_id' => 'main-nav',
//		                        'menu_class' => 'nav navbar-nav sm sm-blue',
//	                        ));?>
	                        <?php shailan_dropdown_menu( array(
			                        'menu_class' => 'nav navbar-nav sm sm-blue',
			                        'menu_id' => 'main-nav',
                                )
                            ); ?>

                        </nav><!-- /.navbar-collapse -->

                    </div><!-- /.container-fluid -->
                </div><!-- /.navbar navbar-default -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.full-main-menu -->
</div><!-- #main-menu -->
<!-- Main Menu end -->

