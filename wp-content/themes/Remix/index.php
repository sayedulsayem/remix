<?php get_header(); ?>
<?php
/*
	Template Name: blog page
 */
?>

    <div class="menu-height-fix">
    </div><!-- /.menu-height-fix -->


    <section id="page-head" class="head-wrap">
        <div class="section-padding overlay text-center">
            <div class="container">
                <div class="heading-txt">
                    <h1>Our Blog</h1>
                    <p>Home <span>/ Blog</span></p>
                </div><!-- /.heading-txt -->
            </div><!-- /.container -->
        </div>
    </section><!-- #page-head -->



    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="post-container">

                    <div class="single-post">
                        <div class="item-md4-head">
                            <div class="date text-center">
                                <span>30</span>
                                sep
                            </div>
                            <h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.error-free strategic theme aret-of-the-box imperatives.</a></h5>
                            <p class="entry-meta">
                                <a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
                                <a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
                                <a href="#"><i class="fa fa-heart-o"></i> 155</a>
                            </p>
                        </div><!-- /.item-md4-head -->

                        <div class="thumbnail-img">
                            <img src="images/post/06.jpg" alt="Post Iamge">
                        </div><!-- /.thumbnail-img -->
                        <div class="entry-content">
                            <p>Error-free strategic theme a great-of-the-box imperatives. Progressively administratdologies with any scalable infomediaries. Assertively error-free strategic theme great aret-of-the-box with imperatives. Progressively administratdologies with tives. Progressively administratdologies with scalable infomediaries. Assertively error-free strategic theme. Assertively free strategic theme</p>
                        </div>
                        <div class="link">
                            <a href="#" class="btn btn-default">Read More</a>
                        </div>
                    </div><!-- /.single-post -->

                    <div class="single-post">
                        <div class="item-md4-head">
                            <div class="date text-center">
                                <span>30</span>
                                sep
                            </div>
                            <h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.error-free strategic theme aret-of-the-box imperatives.</a></h5>
                            <p class="entry-meta">
                                <a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
                                <a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
                                <a href="#"><i class="fa fa-heart-o"></i> 155</a>
                            </p>
                        </div><!-- /.item-md4-head -->

                        <div class="thumbnail-img">
                            <img src="images/post/07.jpg" alt="Post Iamge">
                        </div><!-- /.thumbnail-img -->
                        <div class="entry-content">
                            <p>Error-free strategic theme a great-of-the-box imperatives. Progressively administratdologies with any scalable infomediaries. Assertively error-free strategic theme great aret-of-the-box with imperatives. Progressively administratdologies with tives. Progressively administratdologies with scalable infomediaries. Assertively error-free strategic theme. Assertively free strategic theme</p>
                        </div>
                        <div class="link">
                            <a href="#" class="btn btn-default">Read More</a>
                        </div>
                    </div><!-- /.single-post -->

                    <div class="single-post">
                        <div class="item-md4-head">
                            <div class="date text-center">
                                <span>30</span>
                                sep
                            </div>
                            <h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.error-free strategic theme aret-of-the-box imperatives.</a></h5>
                            <p class="entry-meta">
                                <a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
                                <a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
                                <a href="#"><i class="fa fa-heart-o"></i> 155</a>
                            </p>
                        </div><!-- /.item-md4-head -->

                        <div class="thumbnail-img">
                            <img src="images/post/08.jpg" alt="Post Iamge">
                        </div><!-- /.thumbnail-img -->
                        <div class="entry-content">
                            <p>Error-free strategic theme a great-of-the-box imperatives. Progressively administratdologies with any scalable infomediaries. Assertively error-free strategic theme great aret-of-the-box with imperatives. Progressively administratdologies with tives. Progressively administratdologies with scalable infomediaries. Assertively error-free strategic theme. Assertively free strategic theme</p>
                        </div>
                        <div class="link">
                            <a href="#" class="btn btn-default">Read More</a>
                        </div>
                    </div><!-- /.single-post -->

                    <div class="single-post">
                        <div class="item-md4-head">
                            <div class="date text-center">
                                <span>30</span>
                                sep
                            </div>
                            <h5 class="entry-title"><a href="#">Seamlessly redefstanards  top customer.error-free strategic theme aret-of-the-box imperatives.</a></h5>
                            <p class="entry-meta">
                                <a href="#"><i class="fa fa-user"></i> Malek 1316</a> <span>|</span>
                                <a href="#"><i class="fa fa-comments"></i> 05 Comments</a> <span>|</span>
                                <a href="#"><i class="fa fa-heart-o"></i> 155</a>
                            </p>
                        </div><!-- /.item-md4-head -->

                        <div class="thumbnail-img">
                            <img src="images/post/09.jpg" alt="Post Iamge">
                        </div><!-- /.thumbnail-img -->
                        <div class="entry-content">
                            <p>Error-free strategic theme a great-of-the-box imperatives. Progressively administratdologies with any scalable infomediaries. Assertively error-free strategic theme great aret-of-the-box with imperatives. Progressively administratdologies with tives. Progressively administratdologies with scalable infomediaries. Assertively error-free strategic theme. Assertively free strategic theme</p>
                        </div>
                        <div class="link">
                            <a href="#" class="btn btn-default">Read More</a>
                        </div>
                    </div><!-- /.single-post -->


                    <div class="pagination-list">
                        <div class="pagination">
                            <ul>
                                <li><a href="#" class="angle-left"><i class="fa fa-angle-left"></i></a></li>
                                <li class="active"><a>1</a></li>
                                <li class=""><a href="#">2</a></li>
                                <li class=""><a href="#">3</a></li>
                                <li class=""><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#" class="angle-right"><i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div><!-- /.pagination -->
                    </div><!-- /.pagination-list -->

                </div><!-- /.post-container -->
            </div><!-- /.col-md-8 -->

            <div class="col-md-4">
                <?php
                //wp_nav_menu( array( 'theme_location'	=> 'blog'	) );
                dynamic_sidebar( 'blog' );
                ?>
            </div>

        </div><!-- /.row -->
    </div><!-- /.container -->

<?php get_footer(); ?>